module.exports = Object.freeze({
    BOT_START: "/start",
    CHESS_START: "Start Game",
    CHESS_STOP: "End Game",
    UNDO_MOVE: "Undo Move"
});
