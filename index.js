const TelegramBot = require("node-telegram-bot-api");
require("dotenv").config();
const token = process.env.TOKEN;
const {Chess} = require("chess.js");

const bot = new TelegramBot(token, {polling: true});

// CONSTANTS & FUNCTIONS
const COMMANDS = require("./constants/commands");
const PARAMS = require("./constants/params");
const boardFunctions = require("./functions/board");
const movesFunctions = require("./functions/moves");
const utils = require("./functions/utils");

// STATE
let chessPlaying = {};
let chessGame = {};

const finishGame = (game, chatId, botMove) => {
  if (game.in_checkmate()) {
    botMove ? bot.sendMessage(chatId, "You lost!") : bot.sendMessage(chatId, "You won!");
  }
  if (game.in_draw()) {
    bot.sendMessage(chatId, "Draw!");
  }
  if (game.in_stalemate()) {
    bot.sendMessage(chatId, "Stalemate!");
  }
  if (game.in_threefold_repetition()) {
    bot.sendMessage(chatId, "Repeat three moves");
  }
  bot.sendMessage(chatId, "The game is over", {
    "reply_markup": {
      "keyboard": [
        [COMMANDS.CHESS_START]
      ]
    }
  });
  chessPlaying[chatId] = false;
};

const undoMove = (game, chatId) => {
  if (!chessPlaying[chatId]) {
    return bot.sendMessage(chatId, "Cannot undo the move, because the game is over");
  }

  if (!game.history().length) {
    return bot.sendMessage(chatId, "Cannot undo the move, because the game has just started");
  }

  // Undo last bot`s move
  game.undo();
  // Undo last user`s move
  game.undo();

  const boardImage = boardFunctions.drawImageCanvas(game, {visible: false});
  bot.sendPhoto(chatId, boardImage);
};

bot.on("message", (msg) => {
  try {

    if (!chessGame[msg.chat.id]) {
      chessGame[msg.chat.id] = {};
    }

    // NOT PLAYING & IN COMMANDS LIST
    if (!chessPlaying[msg.chat.id] && Object.values(COMMANDS).indexOf(msg.text) !== -1) {

      if (msg.text === COMMANDS.BOT_START) {
        bot.sendMessage(msg.chat.id, "Chess?", {
          "reply_markup": {
            "keyboard": [
              [COMMANDS.CHESS_START]
            ]
          }
        });
      }

      if (msg.text === COMMANDS.CHESS_START) {
        chessPlaying[msg.chat.id] = true;
        // chessPlaying = true
        chessGame[msg.chat.id] = new Chess();
        bot.sendMessage(msg.chat.id, "Moves notation e2-e4, f2-f3", {
          "reply_markup": {
            "keyboard": [
              [COMMANDS.CHESS_STOP, COMMANDS.UNDO_MOVE],
            ]
          }
        });
        bot.sendMessage(msg.chat.id, "Starting the game");
        const boardImage = boardFunctions.drawImageCanvas(chessGame[msg.chat.id], {visible: false});
        bot.sendPhoto(msg.chat.id, boardImage);
      }

    }

    // CHESS COMMAND
    else if (chessPlaying[msg.chat.id] && utils.isChessCommand(msg.text)) {

      const fenBeforeMove = chessGame[msg.chat.id].fen();
      chessGame[msg.chat.id].move(msg.text.toLowerCase(), {sloppy: true});
      const fenAfterMove = chessGame[msg.chat.id].fen();

      // IF USER MOVE IS POSSIBLE
      if (fenBeforeMove !== fenAfterMove) {

        // IF USER HAS FINISHED THE GAME
        if (chessGame[msg.chat.id].game_over()) {
          finishGame(chessGame[msg.chat.id], msg.chat.id, false);
          const boardImage = boardFunctions.drawImageCanvas(chessGame[msg.chat.id], {visible: true});
          bot.sendPhoto(msg.chat.id, boardImage);
          game.reset();
        }
        // IF BOT CAN MOVE
        else {
          const botMove = movesFunctions.minimaxRoot(PARAMS.depth, chessGame[msg.chat.id], true);
          chessGame[msg.chat.id].move(botMove, {sloppy: true});
          if (chessGame[msg.chat.id].in_check() && !chessGame[msg.chat.id].game_over()) {
            bot.sendMessage(msg.chat.id, "Сheck!");
          }
          // IF BOT HAS FINISHED THE GAME
          if (chessGame[msg.chat.id].game_over()) {
            const boardImage = boardFunctions.drawImageCanvas(chessGame[msg.chat.id], {visible: true});
            bot.sendPhoto(msg.chat.id, boardImage);
            finishGame(chessGame[msg.chat.id], msg.chat.id, true);
            game.reset();
          } else {
            const boardImage = boardFunctions.drawImageCanvas(chessGame[msg.chat.id], {visible: false});
            bot.sendPhoto(msg.chat.id, boardImage);
          }
        }

      }
      // IF USER MOVE IS NOT POSSIBLE
      else {
        bot.sendMessage(msg.chat.id, "Impossible move!");
      }

    }

    // STOP PLAYING
    else if (msg.text === COMMANDS.CHESS_STOP) {
      finishGame(chessGame[msg.chat.id], msg.chat.id, null);
    }

    // UNDO MOVE
    else if (msg.text === COMMANDS.UNDO_MOVE) {
      undoMove(chessGame[msg.chat.id], msg.chat.id);
    }

    // NOT A COMMAND
    else {
      bot.sendMessage(msg.chat.id, "What do you mean?");
    }

  } catch (e) {
    console.log("Error occurred", e);
  }
});
