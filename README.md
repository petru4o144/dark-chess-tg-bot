# Telegram Bot to Play Dark Chess

## How to launch:

Standard NodeJS application installation flow.

Clone repo:
```
git clone https://gitlab.com/petru4o144/dark-chess-tg-bot.git
```    

Go to project directory:
```
cd telegram_chess_bot/
```
  
Install dependencies:
```
npm i
```

Create your bot via command`/newbot` in [@BotFather](https://telegram.me/BotFather). Create in the project root `.env` file and put the obtained token into:
```
TOKEN=123456789:AAEKfs...Dj38g
```

Start the application:
```
npm run start
```

To start in development mode and observe changes in `.js` files:
```
npm run dev
```

  
  
## Available notations:
Bot accepts moves according to the classic notation defining the origin and destination of the figure being played.  

Example:  
```
e2-e4
d7-d6
d2-d4
```

## Modules used:  
* [node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api)
* [chess.js](https://github.com/jhlywa/chess.js)
* [canvas](https://github.com/Automattic/node-canvas)
