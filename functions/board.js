const {Image, createCanvas} = require("canvas");
const params = require("../constants/params");

const squareWidth = 50;
const totalSquares = 64;
const boardSize = 8;
const colorLigth = "#EAD8C3";
const colorDark = "#B08368";

const isEnemyPiece = piece => piece.color === "b";

const isMyPiece = piece => piece.color === "w";

const pieceToSquare = ({x, y}) => `${ params.chessLetters[x] }${ -(y - 8) }`;

const isReachable = ({piece, x_to, y_to, chess}) => {
  if (!isEnemyPiece(piece)) return true;

  const gameBoard = chess.board();
  const desiredPiece = pieceToSquare({x: x_to, y: y_to});

  let i, x, y = -1;

  for (i = 0; i < totalSquares; i++) {
    x++;
    if (i % boardSize === 0) {
      y++;
      x = 0;
    }

    const piece = gameBoard[y][x];
    if (piece !== null && isMyPiece(piece)) {

      const move = chess.move({color: "w", from: pieceToSquare({x, y}), to: desiredPiece});
      if (move) {
        // console.log("move", move);
        chess.undo();
        return true;
      }
    }
  }
};

const drawImageCanvas = (chess, {visible} = {visible: false}) => {
  const gameBoard = chess.board();

  const canvas = createCanvas(400, 400);
  const context = canvas.getContext("2d");
  const img = new Image();

  let i, x, y = -1;

  for (i = 0; i < totalSquares; i++) {
    x++;
    if (i % boardSize === 0) {
      y++;
      x = 0;
    }

    context.beginPath();
    context.rect(x * squareWidth, y * squareWidth, squareWidth, squareWidth);
    context.fillStyle = (x + y) % 2 ? colorDark : colorLigth;
    context.fill();

    const piece = gameBoard[y][x];

    if (piece !== null && ((visible) || (!visible && isReachable({piece, x_to: x, y_to: y, chess})))) {
      img.src = `./imgs/${ piece.color }${ piece.type.toUpperCase() }.svg`;
      context.drawImage(img, x * squareWidth, y * squareWidth, squareWidth, squareWidth);
    }

    context.font = "11px Verdana";
    context.fillStyle = (x + y) % 2 ? colorLigth : colorDark;
    if (x === 0) context.fillText(-(y - 8), x * squareWidth + 1, y * squareWidth + 12);
    if (y === 7) context.fillText(params.chessLetters[x], x * squareWidth + 42, y * squareWidth + 47);

  }

  return Buffer.from(canvas.toDataURL().slice(22), "base64");

};

module.exports.drawImageCanvas = drawImageCanvas;
